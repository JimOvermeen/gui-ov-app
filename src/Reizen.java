public class Reizen {
    private String aankomstPunt;
    private String vertrekPunt;

    public Reizen(String aankomstPunt, String vertrekPunt){
        this.aankomstPunt = aankomstPunt;
        this.vertrekPunt = vertrekPunt;
    }

    public String getAankomstPunt(){
        return aankomstPunt;
    }
    public String getVertrekPunt(){
        return vertrekPunt;
    }
    public void setAankomstPunt(){
        this.aankomstPunt = aankomstPunt;
    }
    public void setVertrekPunt(){
        this.vertrekPunt = vertrekPunt;
    }


}
