import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;


public class GUI {

    public static JFrame OvApp;
    public static JLabel welkomTekst;
    public static JPanel OvAppPanel;
    public static JPanel reisLocatiePanel;
    public static JPanel reisLabelPanel;
    public static JPanel reisBestemmingPanel;
    public static JPanel reisZoekenPanel;
    public static JPanel listReis;
    private Reizen reizen;

    JList<Reizen> list = new JList<>();
    DefaultListModel<Reizen> model = new DefaultListModel<>();

    public GUI() {
        reizen = new Reizen("Deventer", "Amersfoort");
        model.addElement(reizen);
        reizen = new Reizen("Test","test");
        model.addElement(reizen);

        beginScherm();
    }

    public void beginScherm() {

        OvApp = new JFrame("Ov-app");
        OvApp.setSize(500, 500);

        OvAppPanel = new JPanel();

        welkomTekst = new JLabel("Welkom bij de OV-app");

        JButton reisToevoegen = new JButton("Reis toevoegen");
        reisToevoegen.addActionListener(e -> reisZoeken());

        JButton reisInzien = new JButton("Opgeslagen reizen");
        reisInzien.addActionListener(e -> savedReis());

        Box box = Box.createVerticalBox();
        box.add(welkomTekst);
        box.add(reisToevoegen);
        box.add(reisInzien);

        OvAppPanel.add(box);
        OvApp.add(OvAppPanel);
        OvApp.setVisible(true);
        OvAppPanel.setVisible(true);
    }

    private void savedReis() {

        OvAppPanel.setVisible(false);

        JLabel reisOverzichtLabel = new JLabel("Reisoverzicht: Uw reis vertrekt vanaf Deventer en u komt aan in Amersfoort");
        JLabel perronVertrekLabel = new JLabel("Perron : 3, Deventer");
        JLabel perronAankomstLabel = new JLabel("Perron : 5, Amersfoort");


        OvApp.add(reisOverzichtLabel, BorderLayout.NORTH);
        OvApp.add(perronVertrekLabel, BorderLayout.WEST);
        OvApp.add(perronAankomstLabel, BorderLayout.EAST);

    }

    public void reisZoeken() {
        ArrayList<String> stations = new ArrayList<>();
        stations.add("Voer uw station in");
        stations.add("Deventer");
        stations.add("Amersfoort");
        stations.add("Amsterdam");
        OvAppPanel.setVisible(false);

        reisLocatiePanel = new JPanel(new GridBagLayout());
        reisLabelPanel = new JPanel(new GridBagLayout());
        reisBestemmingPanel = new JPanel(new GridBagLayout());
        reisZoekenPanel = new JPanel(new GridBagLayout());
        JLabel reisZoeken = new JLabel("Reis zoeken");
        JComboBox reisVertrekBox = new JComboBox(stations.toArray());
        JComboBox reisAankomstBox = new JComboBox(stations.toArray());
        JButton searchReis = new JButton("Zoek");
        searchReis.addActionListener(e -> listReis(reisVertrekBox.getSelectedItem().toString(), reisAankomstBox.getSelectedItem().toString()));
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.insets = new Insets(10, 10, 10, 10);
        reisLabelPanel.add(reisZoeken, constraints);
        reisLocatiePanel.add(reisVertrekBox, constraints);
        reisBestemmingPanel.add(reisAankomstBox, constraints);
        reisZoekenPanel.add(searchReis, constraints);

        OvApp.add(reisZoekenPanel, BorderLayout.SOUTH);
        OvApp.add(reisBestemmingPanel, BorderLayout.EAST);
        OvApp.add(reisLabelPanel, BorderLayout.NORTH);
        OvApp.add(reisLocatiePanel, BorderLayout.WEST);
    }

    private void listReis(String vertrekLocatie, String aankomstLocatie) {

        reisZoekenPanel.setVisible(false);
        reisBestemmingPanel.setVisible(false);
        reisLabelPanel.setVisible(false);
        reisLocatiePanel.setVisible(false);
        JButton showReisBtn = new JButton("Reis inzien");
        showReisBtn.addActionListener(e -> showReis(vertrekLocatie, aankomstLocatie));

        listReis = new JPanel();
        list.setModel(model);
        System.out.println(model);
        listReis.add(new JScrollPane(list));
        listReis.add(showReisBtn);
        OvApp.add(listReis);
    }

    public void showReis(String vertrekLocatie, String aankomstLocatie) {
        listReis.setVisible(false);

        JPanel reisHeaderPanel = new JPanel(new GridBagLayout());
        JPanel perronVertrekPanel = new JPanel(new GridBagLayout());
        JPanel perronAankomstPanel = new JPanel(new GridBagLayout());
        JPanel saveReisPanel = new JPanel(new GridBagLayout());
        JPanel showMeldingPanel = new JPanel(new GridBagLayout());

        JLabel reisOverzichtLabel = new JLabel("Reisoverzicht: Uw reis vertrekt vanaf " + vertrekLocatie + " en u komt aan in " + aankomstLocatie);
        JLabel perronVertrekLabel = new JLabel("Perron : 3, " + vertrekLocatie);
        JLabel perronAankomstLabel = new JLabel("Perron : 5, " + aankomstLocatie);
        JButton saveReis = new JButton("Sla reis op");
        saveReis.addActionListener(e -> {
            OvApp.dispose();
            beginScherm();
        });
        JButton showMeldingButton = new JButton("Laat melding zien");
        showMeldingButton.addActionListener(e -> testMap());

//        Laat een melding zien
//        showMeldingButton.addActionListener(e -> showMelding(OvApp));

        GridBagConstraints constraints = new GridBagConstraints();
        constraints.insets = new Insets(10, 10, 10, 10);
        reisHeaderPanel.add(reisOverzichtLabel, constraints);
        perronVertrekPanel.add(perronVertrekLabel, constraints);
        perronAankomstPanel.add(perronAankomstLabel, constraints);
        saveReisPanel.add(saveReis, constraints);
        showMeldingPanel.add(showMeldingButton, constraints);

        OvApp.add(saveReisPanel, BorderLayout.SOUTH);
        OvApp.add(showMeldingPanel, BorderLayout.CENTER);
        OvApp.add(perronVertrekPanel, BorderLayout.WEST);
        OvApp.add(perronAankomstPanel, BorderLayout.EAST);
        OvApp.add(reisHeaderPanel, BorderLayout.NORTH);
    }
    //geeft een error
    private void showMelding(JFrame frame) {
        JOptionPane.showMessageDialog(frame, "Je trein vertrekt over 10 minuten!!!!");
    }

    //laat google maps zien
    private void testMap(){
        try {
            Desktop.getDesktop().browse(new URI("https://www.google.com/maps/dir/Amersfoort+Centraal,+Stationsplein,+Amersfoort/Deventer,+Stationsplein+7,+7411+HB+Deventer/@52.2061474,5.6256371,11z/data=!3m1!4b1!4m14!4m13!1m5!1m1!1s0x47c64402ac66f94d:0x7d5aa9b22196b161!2m2!1d5.3740553!2d52.153438!1m5!1m1!1s0x47c7ea4277826887:0x5fd443bc63b56998!2m2!1d6.1606853!2d52.2570809!3e3"));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (URISyntaxException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }



}

